using System.Threading.Tasks;

namespace Truyol.Console.Hosting
{
    public interface IConsoleApp
    {
        Task<int> RunAsync(string[] args);
    }
}