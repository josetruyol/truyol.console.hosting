using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Truyol.Console.Hosting
{
    public interface IStartup
    {
        IConfiguration LoadConfiguration();
        void ConfigureServices(IServiceCollection serviceCollection, IConfiguration config);
    }
}