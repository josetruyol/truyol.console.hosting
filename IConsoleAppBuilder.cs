using System;
using System.Threading.Tasks;

namespace Truyol.Console.Hosting
{
    public interface IConsoleAppBuilder
    {
        IConsoleAppBuilder Build();
        IConsoleAppBuilder UseStartup(IStartup startup);
        Task<int> RunAsync();
    }
}