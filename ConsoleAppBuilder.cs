﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Truyol.Console.Hosting
{
    public class ConsoleAppBuilder : IConsoleAppBuilder
    {
        private IStartup _startup;
        private ServiceProvider _serviceProvider;
        private string[] _args;

        public ConsoleAppBuilder(string[] args)
        {
            _args = args;
        }

        public IConsoleAppBuilder Build()
        {
            var serviceCollection = new ServiceCollection();
            if (_startup != null)
            {
                IConfiguration config = _startup.LoadConfiguration();
                _startup.ConfigureServices(serviceCollection, config);
            }
            _serviceProvider = serviceCollection.BuildServiceProvider();
            return this;
        }

        public async Task<int> RunAsync()
        {
            int result = await _serviceProvider.GetService<IConsoleApp>().RunAsync(_args);
            _serviceProvider.Dispose();
            return result;
        }

        public IConsoleAppBuilder UseStartup(IStartup startup)
        {
            _startup = startup;
            return this;
        }

    }
}